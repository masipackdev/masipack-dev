#include "totvs.ch"
#include "fwmvcdef.ch"

/*/
@Program: RFISG004
@Description: Atualiza a descri��o cient�fica (SB5) ap�s preenchida a descri��o do produto (B1_DESC)
@Date: 14.10.2019
@Author: Espec�fico Masipack
/*/

User Function RFISG004()

Local oModel	:= FWModelActive()
Local oModelSB5 := oModel:GetModel("SB5DETAIL")
Local oView  := FWViewActive()

Default INLCUI := .T.

   	If !(oModelSB5 == Nil) .And. INCLUI
        oModelSB5:LoadValue("B5_CEME", oModel:GetValue("SB1MASTER","B1_DESC"))
        oView:Refresh()
    Endif
        
Return .T.