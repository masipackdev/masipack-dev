#Include "rwmake.ch"   
#Include "Topconn.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �LP520DB   �Autor  � Cecilia            � Data �  18/06/08   ���
�������������������������������������������������������������������������͹��
���Desc.     �Programa para retorno de conta contabil em lancamento padro-���
���          �nizado 520 - sequencia 05  - Debito                         ���
�������������������������������������������������������������������������͹��
���Uso       �Masipack                                                    ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/    

User Function LP520DB(_nSeq)

Private  _cCli,_cContaCr,_cContaDb,_cHist,_nValor,_cTpDoc

_cAliasold:= alias()
_cConta   := ""
_cTpDoc   := SE1->E1_TIPO
_cSeq     := Alltrim(CT5->CT5_SEQUEN)
_cCli     := SE1->E1_CLIENTE
_cLoja    := SE1->E1_LOJA 
_cTpCli   := " "
_cTpLanc   := Alltrim(CT5->CT5_DC)

// Contabiliza se for Cliente de exportacao com variacao cambial

If _nSeq == 5
   SA1->(DbSetOrder(1))
   If SA1->(DbSeek(xFilial("SA1")+_cCli+_cLoja))
	  _cTpCli := SA1->A1_EST
   EndIf  
   
	If _cTpDoc = "RA" .and. _cTpCli = "EX"
		If SE1->E1_ACRESC > 0
			_cConta := "370010014"
		ElseIf SE1->E1_DECRESC > 0
			_cConta := "110400002"
		Endif
	Endif
	   
ElseIf _nSeq == 4
//		If (SE5->E5_NATUREZ) == "V14"   - ALTERADO 08/01/16 - LIZANDRA
	If (SE5->E5_MOTBX) == "FAT"
		_cConta := "110400008"

	//VICTOR DESSUNTE - 07/10/2016
	//INICIO - TICKET: 2016091937000074
	ElseIf SE5->E5_MOTBX == 'DEB'
		SA1->(DbSetOrder(1))
		SA1->(DbSeek(xFilial("SA1")+SE5->E5_CLIENTE+SE5->E5_LOJA))
		If ALLTRIM(SE5->E5_NATUREZ) == 'L01'
			If SA1->A1_EST == 'EX'
				_cConta := "220900003"
			Else
				_cConta := "220900001"
			EndIf
		Else
			_cConta := SA1->A1_CONTA
		EndIf
 	//FIM - TICKET: 2016091937000074
	Else
		_cConta := SA6->A6_CONTA	
 	EndIf
ElseIf _nSeq == 3
	//VICTOR DESSUNTE - 17/10/2016
	//INICIO - TICKET: 2016091937000074
	If SE5->E5_MOTBX == 'DEB' .AND. ALLTRIM(SE5->E5_NATUREZ) == 'L01'
		_cConta := 'C'+_cCli
 	EndIf
 	//FIM - TICKET: 2016091937000074
Endif 
 
Return(_cConta)
