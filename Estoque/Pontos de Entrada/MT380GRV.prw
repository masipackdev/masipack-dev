#include 'totvs.ch'
#include 'protheus.ch'

/*/{Protheus.doc} User Function MTAGRSD4
Ponto de Entrada executado enquanto ocorre a grava��o dos empenhos na tabela SD4
(N�o � necess�rio usar o reclock, pois o mesmo est� ativo na fun��o padr�o.)
@type  Function
@author Masipack
@since 21/09/2020
/*/
User Function MTAGRSD4()

Local cTRB  := GetNextAlias()
Local cRef  := POSICIONE('SC2',1,FWxFilial('SC2') + M->D4_OP, 'C2_MSREFER')

    IF INCLUI .And. !Empty(cRef)

        Replace D4_MSREFER With cRef
    
        BEGINSQL ALIAS cTRB
        
        SELECT  SUM(D4_QTDEORI)[QTDEORI], 
                SUM(D4_QUANT)[QUANT] 
        FROM %Table:SD4% SD4
        WHERE D4_FILIAL = %xFilial:SD4%
        AND D4_MSREFER = %Exp:cRef% 
        AND SD4.%NOTDEL%
        
        ENDSQL

        If cTRB->(!EOF()) .And. !(cTRB->QTDEORI == cTRB->QUANT)
            Replace D4_MSLF With 'X'
        Endif

        cTRB->(dbCloseArea())

    Endif

Return
