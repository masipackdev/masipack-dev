#include 'protheus.ch'

/*/{Protheus.doc} User Function MT241LOK
(long_description)
@type  Function
@author E.DINIZ - [ DS2U ]
@since 05/02/2021
@version version
/*/
User Function MT241LOK()

Local _aArea    := {SD3->(GetArea()), SD4->(GetArea())}
Local _cLF      := ''
Local _nPos     := PARAMIXB[1]
Local _nPosRef  := aScan(aHeader,{|x| Alltrim(x[2]) == "D3_MSREFER"})
Local _nPosPrd  := aScan(aHeader,{|x| Alltrim(x[2]) == "D3_COD"})
Local _lDeleta  := aCols[_nPos,Len(aCols[_nPos])]
Local _lRet     := .T.
    
    If cEmpAnt $ '01|10'
        SD4->(dbSetOrder(11)) //D4_FILIAL + D4_MSREFER + D4_COD
        SD4->(dbSeek(FWxFilial('SD4') + aCols[_nPos,_nPosRef] + aCols[_nPos,_nPosPrd]))
        _cLF := IIF(_lDeleta,'X',SPACE(TamSX3('D4_MSREFER')[1]))

        If SD4->(FOUND())
            Reclock('SD4',.F.)
            Replace D4_MSLF WITH _cLF
            SD4->(MsUnlock())
        EndIf
    Endif

    AEval(_aArea, {|x| RestArea(x)})

Return _lRet
