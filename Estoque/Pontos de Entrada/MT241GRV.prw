#include "totvs.ch"

/*/{Protheus.doc} User Function MT241GRV
LOCALIZA��O:  Fun��o A241GRAVA (Grava��o do movimento)
EM QUE PONTO: Ap�s a grava��o dos dados (aCols) no SD3, e tem a finalidade de atualizar algum arquivo ou campo.
@type  User Function
@author Masipack
@since 29/11/2019
@version version
@param  PARAMIXB[1] = N�mero do Documento
        PARAMIXB[2] = Vetor bidimensional com nome campo/valor do campo (somente ser� enviado se o Ponto de Entrada MT241CAB for utilizado)
@return Nil
@see https://tdn.totvs.com/display/public/PROT/MT241GRV+-+Atualiza+arquivo+ou+campo
/*/
User Function MT241GRV()

Local _aAreaSD3 := SD3->(GetArea())
Local _aAreaSD4 := SD4->(GetArea())
Local _aAreaSB1 := SB1->(GetArea())
Local _aList    := aClone(aCols)
Local _nI       := 0

    IF !(SUBSTR(cEmpAnt,1,2) == "15")
    
        SD3->(dbSetOrder(1))    //D3_FILIAL+D3_OP+D3_COD+D3_LOCAL
        SD4->(dbSetOrder(1))    //D4_FILIAL+D4_COD+D4_OP+D4_TRT+D4_LOTECTL+D4_NUMLOTE

        FOR _nI := 1 TO LEN(_aList)

            //***************************************
            //* Grava os campos espec��ficos (SD4)   *
            //***************************************
            /*
            IF SD4->(dbSeek(FWxFilial("SD4")+_aList[_nI][2]+_aList[_nI][1]))
                Reclock("SD4",.F.)
                SD4->D4_MSLF := "X"
                SD4->(MsUnlock())
            ENDIF
            */

            //***************************************
            //* Grava os campos espec�ficos (SD3)   *
            //***************************************
            SD3->(dbGoTop())
            IF SD3->(dbSeek(FWxFilial("SD3") + _aList[_nI][1] + _aList[_nI][2] + _aList[_nI][8]))
                WHILE SD3->(!EOF()) .And. SD3->D3_FILIAL + SD3->D3_OP + SD3->D3_COD + SD3->D3_LOCAL == FWxFilial("SD3") + _aList[_nI][1] + _aList[_nI][2] + _aList[_nI][8]
                    IF Empty(SD3->D3_ESTORNO)
                        Reclock("SD3",.F.)
                        SD3->D3_MSREFER := POSICIONE("SD4",1,FWxFilial("SD4")+_aList[_nI][2]+_aList[_nI][1],"D4_MSREFER")
                        SD3->D3_MSLOCAL := POSICIONE("SB1",1,FWxFilial("SB1")+_aList[_nI][2],"B1_LOCAL")
                        SD3->(MsUnlock())
                    ENDIF
                    SD3->(dbSkip())
                ENDDO
            ENDIF
            
        NEXT _nI

        //***************************************
        //* Restauro a origem dos ponteiros     *
        //***************************************
        RestArea(_aAreaSD3)
        RestArea(_aAreaSD4)
        RestArea(_aAreaSB1)
    
    ENDIF
    
Return