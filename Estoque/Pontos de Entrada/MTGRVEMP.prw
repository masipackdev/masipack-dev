#include 'totvs.ch'
#include 'protheus.ch'

/*/{Protheus.doc} User Function MTGRVEMP
    Ponto de entrada que tem o objetivo de manipular as informa��es que ser�o gravadas 
    para cada item de empenho gerado, possibilitando alterar um produto empenhado, 
    o local padr�o do produto, ou ainda, selecionar um outro lote/sublote para o produto, 
    de acordo com a necessidade.Ex.: Possibilita gravar o conte�do dos campos customizados 
    que forem adicionados na tela de altera��o de empenhos, registrando as informa��es na 
    tabela SD4.
    
    LOCALIZA��O: Function GravaEmp - Fun��o principal respons�vel pelo tratamento na gera��o de 
                 empenhos, no final da Fun��o, ap�s ter executado todos os processos envolvidos 
                 na grava��o dos empenhos.

@type  Function
@author Masipack
@since 14/12/2020
@version version
@param param_name, param_type, param_descr
        Nome			Tipo			Descri��o
        PARAMIXB[1]		Caracter		C�digo do Produto que ser� gravado
        PARAMIXB[2]		Caracter		Armaz�m selecionado para o produto
        PARAMIXB[3]		Num�rico		Quantidade do produto a ser gravada no empenho
        PARAMIXB[4]		Num�rico		Quantidade na Segunda Unidade de Medida
        PARAMIXB[5]		Caracter		N�mero do lote que ser� gravado
        PARAMIXB[6]		Caracter		N�mero do sub-lote que ser� gravado
        PARAMIXB[7]		Caracter		Endere�o do produto
        PARAMIXB[8]		Caracter		N�mero de S�rie do produto
        PARAMIXB[9]		Caracter		N�mero da OP
        PARAMIXB[10]	Caracter		Sequ�ncia do produto na estrutura
        PARAMIXB[11]	Caracter		N�mero do Pedido de Vendas
        PARAMIXB[12]	Caracter		Item do Pedido de Vendas
        PARAMIXB[13]	Caracter		Origem do Empenho
        PARAMIXB[14]	L�gico			Vari�vel L�gica - Determina se a opera��o � um estorno
        PARAMIXB[15]	Vetor			Vetor de campos que foi carregado na altera��o de empenhos
        PARAMIXB[16]	Num�rico		Posi��o do elemento do vetor de campos
@return (Nulo)
@example
(examples)
@see (links_or_references)
    https://tdn.totvs.com.br/pages/releaseview.action?pageId=6087835
/*/
User Function MTGRVEMP()

Local aArea     := SB2->(GetArea())
Local _cTMP     := GetNextAlias()
Local cProduto  := PARAMIXB[01]
Local cArmazem  := PARAMIXB[02]
Local cNumOP    := PARAMIXB[09]
Local lEstorno  := PARAMIXB[14]
Local lGravaLF  := .F.
Local nQuant    := PARAMIXB[03]

    If cEmpAnt $ '01|' .And. !lEstorno .And. !FUNNAME() $ 'RPCPA011|MATA215'

        dbSelectArea("SB2")
        SB2->(dbSetOrder(1))
        IF SB2->(dbSeek(FWxFilial("SB2") + cProduto + cArmazem))
            lGravaLF := SaldoSB2( ,.F.) >= nQuant
        Endif

        BEGINSQL ALIAS _cTMP
            SELECT B1_GRUPO, D4_MSREFER 
            FROM %Table:SB1% B1, %Table:SD4% D4
            WHERE B1_FILIAL = %Exp:xFilial("SB1")%
            AND B1_COD = D4_COD
            AND B1_GRUPO IN ( SELECT B1_GRUPO FROM %Table:SB1% WHERE B1_COD = %Exp:cProduto% AND %NOTDEL% )
            AND B1.%NOTDEL%
            AND D4_FILIAL = %Exp:xFilial("SD4")%
            AND D4_OP = %Exp:cNumOP%
            AND D4.%NOTDEL%
            GROUP BY B1_GRUPO, D4_MSREFER
        ENDSQL

        IF !Empty((_cTMP)->D4_MSREFER)
            If !lGravaLF .And. FwAlertYesNo('Este produto n�o possui saldo em estoque e j� foi separado. Marc�-lo para a Lista de Falta?')
                Reclock('SD4',.F.)
                SD4->D4_MSLF    := 'X'
                SD4->(MsUnlock())
            Endif
        ENDIF

        (_cTMP)->(dbCloseArea())

    Endif

    RestArea(aArea)

Return
