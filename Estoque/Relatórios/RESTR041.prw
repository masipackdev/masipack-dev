#include 'totvs.ch'
#include 'protheus.ch'


/*/{Protheus.doc} User Function RESTR041
Relat�rio de Saldo de Produtos x Locais (Ser� descontinuado quando o sistema possuir controle de endere�amento)
@type  Function
@author Masipack
@since 13/08/2020
/*/
User Function RESTR041()

Local cPerg	:= "XRESTR041"
Local oReport

	If Pergunte(cPerg,.T.)
        oReport := ReportDef(cPerg)
        oReport:PrintDialog()
    Endif

Return


/*/{Protheus.doc} ReportDef(cPerg)
Defini��o do layout do relat�rio
@type  Static Function
@author Masipack
@since 13/08/2020
/*/
Static Function ReportDef(cPerg)

Local cDesc     := "Este relat�rio imprimir� os produtos com saldos por Local do Estoque"
Local cTitulo   := "Saldos por Local"
Local oReport
Local oSection

    oReport := TReport():New("RESTR041",cTitulo,cPerg,{|oReport| PrintReport(oReport)},cDesc)
	oReport:DisableOrientation()
	oReport:SetLandscape() 
	oReport:oPage:setPaperSize(10)
	oReport:nFontBody := 7
	oReport:cFontBody := "Courier New"

    oSection := TRSection():New(oReport,"CABEC",{"SB1"})
    TRCell():New(oSection,"B1_LOCAL"    ,"SB1"      ,RetTitle("B1_LOCAL")       ,PesqPict("SB1","B1_LOCAL")         ,TamSX3("B1_LOCAL")[1]  )
    TRCell():New(oSection,"B1_COD"      ,"SB1"      ,RetTitle("B1_COD")         ,PesqPict("SB1","B1_COD")           ,TamSX3("B1_COD")[1]    )
    TRCell():New(oSection,"B1_DESC"     ,"SB1"      ,RetTitle("B1_DESC")        ,PesqPict("SB1","B1_DESC")          ,TamSX3("B1_DESC")[1]    )
    TRCell():New(oSection,"B1_UM"       ,"SB1"      ,RetTitle("B1_UM")          ,PesqPict("SB1","B1_UM")            ,TamSX3("B1_UM")[1]     )
    TRCell():New(oSection,"B1_LOCPAD"   ,"SB1"      ,"Armaz�m"                  ,PesqPict("SB1","B1_LOCPAD")        ,TamSX3("B1_LOCPAD")[1] )
    TRCell():New(oSection,"B2_QATU"     ,"SB2"      ,RetTitle("B2_QATU")        ,PesqPict("SB2","B2_QATU")          ,TamSX3("B2_QATU")[1]   )

Return oReport



/*/{Protheus.doc} PrintReport
Impress�o dos dados no relat�rio
@type  Static Function
@author Masipack
@since 13/08/2020
/*/
Static Function PrintReport(oReport)

Local cLocal    := ''
Local cFilter   := ''
Local oSection  := oReport:Section(1)

Default MV_PAR01 := ''              // Produto de
Default MV_PAR02 := 'ZZZZZZ'        // Produto at�
Default MV_PAR03 := ''              // Tipo de Produto de
Default MV_PAR04 := 'ZZ'            // Tipo de Produto at�
Default MV_PAR05 := ''              // Local de
Default MV_PAR06 := 'ZZZZZZ'        // Local at�
Default MV_PAR07 := 2             // Lista Produto sem Local? 1=Sim; 2=N�o

    cFilter := "SB1->B1_COD >= '" + MV_PAR01 + "' .AND. SB1->B1_COD <= '" + MV_PAR02 + "' "
    cFilter += ".AND. SB1->B1_TIPO >= '" + MV_PAR03 + "' .AND. SB1->B1_TIPO <= '" + MV_PAR04 + "' "
    cFilter += ".AND. SB1->B1_LOCAL >= '" + MV_PAR05 + "' .AND. SB1->B1_LOCAL <= '" + MV_PAR06 + "' "
    cFilter += IIF(MV_PAR07 == 2, ".AND. !EMPTY(MV_PAR07) ", "")
    

    dbSelectArea('SB1')
    SB1->(dbOrderNickName("MASSB1LOC"))
    SB1->(dbSetFilter({|| &(cFilter) }, cFilter))
    SB1->(dbGoTop())

    dbSelectArea('SB2')
    SB2->(dbSetOrder(1))

    If SB1->(EOF())
        
        oSection:Cell("B1_LOCAL"):Disable()
        oSection:Cell("B1_COD"):Disable()
        oSection:Cell("B1_DESC"):Disable()
        oSection:Cell("B1_UM"):Disable()
        oSection:Cell("B1_LOCPAD"):Disable()
        oSection:Cell("B2_QATU"):Disable()

        oReport:PrintText( "N�o h� dados, verifique os parametros informados" )
        oSection:Finish() 	
        oReport:EndPage()

    Else

        oReport:SetMeter(0)
        oSection:Init()

        While SB1->(!EOF())
            oReport:IncMeter()

            If !(cLocal == SB1->B1_LOCAL)
                oReport:SkipLine()
                oReport:PrintText("Local: " + ALLTRIM(SB1->B1_LOCAL), oReport:Row())
                oReport:SkipLine()
                oReport:ThinLine()
                oReport:SkipLine()
            Endif
            
            oSection:Cell("B1_LOCAL"):Disable()
            
            IF SB2->(dbSeek( FWxFilial("SB2") + SB1->B1_COD + SB1->B1_LOCPAD ))
                oSection:Cell("B2_QATU"):SetValue( SaldoSB2(,.F.) )
            ELSE
                oSection:Cell("B2_QATU"):SetValue( 0 )
            ENDIF

            If !(cLocal == SB1->B1_LOCAL)
                cLocal := SB1->B1_LOCAL
            Endif

            oSection:PrintLine()

            SB1->(dbSkip())
        Enddo

        oSection:Finish()

    Endif

Return 
