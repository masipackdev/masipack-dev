#include "protheus.ch"

/*/
{protheus.doc}
@Description: Fonte utilizado no ponto de entrada MT120GRV para enviar e-mails de prazo de entregas alterados.
@Author: Everton Diniz
@Date: 16.10.2019
@version: 1.0
@param:
/*/
User Function RCOMG014(cNumPed, lInclui, lAltera, lExclui)

Local aArea     := GetArea()
Local aMSPV     := {}
Local cFornece  := cA120Forn
Local cLoja     := cA120Loj
Local cNome     := ""
Local nPosItem  := 0
Local nPosMSPV  := 0
Local nX        := 0

    If cFornece + cLoja == "00000400"
        
        cNome := Alltrim(POSICIONE("SA2", 1, FWxFilial("SA2") + cFornece + cLoja, "A2_NREDUZ"))
        nPosMSPV := aScan(aHeader,{|x| Alltrim(x[2]) == "C7_MSPV"   })
        nPosItem := aScan(aHeader,{|x| Alltrim(x[2]) == "C7_ITEM"   })
        
        Do Case
        
            Case lInclui .Or. lAltera
                For nX := 1 to len(aCols)
                    If !Empty(aCols[nX,nPosMSPV]) .And. !(aCols[nX,Len(aHeader)+1])
                        AADD(aMSPV,{aCols[nX,nPosMSPV], cNumPed + aCols[nX,nPosItem]})
                    Elseif !Empty(aCols[nX,nPosMSPV]) .And. aCols[nX,Len(aHeader)+1]
                        AADD(aMSPV,{aCols[nX,nPosMSPV],Space(10)})
                    Endif
                Next

            OtherWise
                For nX := 1 to len(aCols)
                    If !Empty(aCols[nX,nPosMSPV])
                    AADD(aMSPV,{aCols[nX,nPosMSPV],Space(10)})
                    Endif
                Next

        EndCase

        U_MSATUPEV(aMSPV,cNome)

    Endif
    
    RestArea(aArea)

Return .T.
